# 面接対策スクリプト

## 想定環境

- windows cygwin(Win10 32bitでは確認済)

- Mac OS(Sierraで確認済)

- Linux

- gitが使えること、gccやg++、makeなどC言語ビルド環境が整っていること

---

## インストールの流れ

まずは、音声合成の準備をする。

Macの場合は、sayコマンドが予め用意されている為、1〜2の作業は不要。

1. HTSランタイムエンジンをインストール

2. Open JTalkをインストール

3. スクリプトのコンフィグをセットアップ

4. 想定質問を入力

5. スクリプト実行

---

### 1. HTSランタイムエンジンをインストール

[サイト](http://hts-engine.sourceforge.net)からソースコードをダウンロードし、

作業フォルダ(例では~/Mensetsu)に置き、コンパイルする。

```
 $ cd  ~/Mensetsu

 $ tar  zxvf hts_engine_API-****.tar.gz

 $ cd  hts_engine_API-*****

 $ ./configure

 $ make
```

次に、以下のファイルを/usr/localの各フォルダにコピーする。

フォルダがない場合は、新規作成する。

```
    $ cp bin/HTS_engine.exe /usr/local/bin/HTS_engine.exe

    $ cp lib/libHTSengine.a /usr/local/bin/libHTSengine.a

    $ cp include/HTS_engine.h /usr/local/bin/HTS_engine.h
```

※この作業を作業を怠ると、Open JTalkのconfigureで失敗するので忘れないこと！

---

### 2. Open JTalkをインストール

[サイト](http://open-jtalk.sourceforge.net)から以下をダウンロードする。

- 本体のソースコード

- 辞書バイナリ(WinでもUTF-8にする)

- HTSボイスバイナリ

作業フォルダ(例では~/Mensetsu/)にそれぞれ置く。

Open JTalkもコンパイルする必要がある。

```
    $ cd  ~/Mensetsu

    $ tar  zxvf Open_JTalk-*****.tar.gz

    $ cd  Open_JTalk-*****

    $ ./configure

    $ make
```

辞書バイナリとHTSボイスバイナリも同フォルダで解凍する


```
    $ tar  zxvf Open_JTalk_dic*****.tar.gz

    $ tar  zxvf HTS_voice*****.tar.gz
```

---

### 3. スクリプトのコンフィグをセットアップ


まず、音声再生が正しく行えるかチェックする。

Open JTalkは入力された文字列を音声wavファイルに変換するところまで行うので、

コマンドラインからwavファイルを再生できる環境を用意する。

* Linuxならaplayコマンドなどがあるらしい。

* Winの場合、[ここ](http://www.cepstrum.co.jp/download/recplay/recplay.html)から、play.exeをダウンロードすると楽。

一連のコマンドは以下のようになる。

```
    $ echo "では、自己紹介を10分間お願いします" | open_ jtalk -x <<辞書ファイルが置いてあるパス>> -m <<nitech_jp_atr503_m001.htsvoiceのパス>> -ow test.wav ; ./play.exe test.wav
```

まずはスクリプトをクローンする。
```
    $ git clone https://gitlab.com/InabaArato/Mensetsu.git
```


音声再生を確認したら、面接対策スクリプトに組み込む。

Macならば、特に変更なく使えると思う。

Open JTalkの場合は、各ファイルやパスを自身の環境に合わしておく。

---

### 4. 想定質問を入力

q.txtに、想定される質問を１行ずつ入力する。

---

### 5. スクリプト実行

スクリプトを実行する。

```
    $ ./mensetsukan_say.sh
```

- q.txtに入力された想定質問を一定間隔に再生する。

- 想定質問はランダムで再生される。

- 止めたい場合は、[enter]キーを押すと止まる。

