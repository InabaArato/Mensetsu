#!/bin/bash

##
# 面接対策用シェルスクリプト OpenJTalk版
#
# 音声合成コマンド(Macならsay、LinuxならOpenJTalkなど)を用いて
# 一定時間ごとに決められた質問を喋ってくれます。
#
# (Mac)
# say "では、自己紹介を10分間お願いします。"
# 
# (OpenJTalk cygwinの場合)
# echo "では、自己紹介を10分間お願いします。" | \
# open_jtalk.exe -x <<辞書ファイルのパス>> -m <<nitech_jp_atr503_m001.htsvoiceのパス>> -ow test.wav ; \
# play.exe test.wav
#
#
# [準備]
#   このスクリプトのディレクトリにq.txt(質問)を用意しておく
#
# [使い方]
#   実行方法
#   $ ./mensetsukan.sh
#
#   終了するには
#   [enter]を押せば終了
#
#

# 音声周りの設定
OPENJTALK=./open_jtalk.exe
DIC_PATH=./open_jtalk_dic***
HTS_VOISE=./nitech_jp_atr503_m001.htsvoice
PLAY_CMD=play.exe


## コンフィグ
Q_FILE_NAME=q.txt
ANS_TIME=15




## グローバル変数
Q_FLG=0



# 必要なファイルが揃っているかチェックする
if [ -e ${Q_FILE_NAME} ]
then
    Q_FLG=1
fi


# 必要なファイルが揃っているなら面接開始！
if [ ${Q_FLG} -eq 1 ]
then
    # 何かが入力されるまでループする
    echo 'start!'
    key="default"
    cnt=1
    while [ -n "${key}" ]
    do
        # 行数を取得(質問ファイルを更新しながらでも使えるように毎回取得)
        q_num=`wc -l ${Q_FILE_NAME} | awk '{split($1, array, " ");print array[1];}'`

        # どの行を読み上げるか乱数で決める
        read_num=$(($RANDOM % ${q_num}))

        # 空行でなければ、その行を読み上げる
        read_line=`sed -n ${read_num}p ${Q_FILE_NAME}`
        if [ -n "${read_line}" ]
        then
            echo Q${cnt}: ${read_line}
            echo ${read_line} | ${OPENJTALK} -x ${DIC_PATH} -m ${HTS_VOISE} -ow test.wav ; ${PLAY_CMD} test.wav

            cnt=`expr ${cnt} + 1`

            # 回答用待ち時間...次に行く前に3秒のカウントダウン
            read -t ${ANS_TIME} key
            if [ -n "${key}" ]
            then
                printf "3" ; sleep 1
                printf "\b2" ; sleep 1
                printf "\b1" ; sleep 1
                printf "\b \n"
            else
                # すぐに待ちを終了する
                # ここに入るとkeyに値が入っているのでループを抜ける
                echo 'end!'
            fi
        fi
    done
fi


